const debug = require('debug')('server')
const chalk = require('chalk')
const helmet = require('helmet')
const cors = require('cors')
const express = require('express')
const { EJSON } = require('bson')
const { MongoClient, MongoError } = require('mongodb')

const uri = 'mongodb://localhost:27017' // Inside the container

main()

async function main () {

	try {
		const client = await new MongoClient(uri)
		await client.connect()
		await client.db('admin').command({ ping: 1 }) // Will fail if no connection
		
		const db = client.db('test_db')
		
		const app = express()
		const cors_options = {}
		
		app.use(cors(cors_options))
		app.use(helmet())
		app.use(express.json())
		app.set('json spaces', 2)
		
		app.get('/', express.static('public'))
		app.get('/collections', async (req, res) => {
			try {
				collAry = await db.listCollections().toArray()
				collections = await collAry.map( c => c.name )
				res.status(200).json(collections)
			} catch (err) {
				res.status(500).json(err)
			}
		})
		app.post('/test/:coll', async (req, res) => {
			try {
				insertResult = await db.collection(req.params.coll).insertOne(req.body)
				response = {}
				response[req.params.coll] = insertResult
				res.status(200).json(response)
			} catch (err) {
				res.status(500).json(err)
			}
		})
		app.get('/exit', async (req, res) => {
			console.info(chalk.yellow('exit requested'))
			process.exit(0)
		})
		
		const port = 4001
		app.listen(port, () => {
			console.info(chalk.cyan(`\nListening on port ${chalk.yellow(port)}`))
		})
		
	} 
	catch (err) {
		console.error(chalk.yellow( err ))
	}
	
}
