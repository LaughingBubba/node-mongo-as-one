# FROM node:16
FROM mongo:5.0
RUN apt-get -y update
RUN apt-get install -y curl node-gyp
RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash -
RUN apt-get install -y nodejs
RUN node -v
RUN npm --version

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

RUN chmod 777 ./dual_process.sh

EXPOSE 4001
# EXPOSE 27017
# CMD [ "node", "./src/index.js" ]
CMD ["./dual_process.sh"]