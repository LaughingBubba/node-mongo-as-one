# node mongo as one

A docker file running Node (express) and MongoDB in the one image / container


# Install and Run
Assumptions:
- Docker Engine on your host machine
- cURL is already installed

Install and run:
- Clone this repo and `cd` into it
- `docker build -t nongo .` to build
- `docker run -it --rm -p 4001:4001 nongo` to launch. NB: Waits 5 secs before starting express
- See [cURL commands below](#curl)


# Docker

$ docker build -t nongo .
 - OR -
$ docker build --no-cache -t nongo .
$ docker build --progress=plain -t nongo .
 
$ docker run -it --rm -p 4001:4001 nongo

## cURL
Run these cURL commands
$ curl -H "Content-Type: application/json" -d '{"hello":"world"}' http://localhost:4001/test/blah
$ curl -H "Content-Type: application/json" -d '{"hello":"world"}' http://localhost:4001/test/borris
$ curl -H "Content-Type: application/json" -d '{"hello":"world"}' http://localhost:4001/test/lol
$ curl -H "Content-Type: application/json" http://localhost:4001/collections

# Links of Interest
https://docs.docker.com/config/containers/multi-service_container/   
https://runnable.com/docker/rails/run-multiple-processes-in-a-container    
https://stackoverflow.com/questions/54090119/how-to-run-a-nodejs-app-in-a-mongodb-docker-image   
https://stackoverflow.com/questions/41540730/compose-two-docker-image-into-one-nodejs-and-mongodb   
