#!/bin/bash

# Start the first process - Mongo
mongod &
  
echo 'Giving Mongo 5 secs to get sorted'
sleep 5

# Start the second process - Node Express App
node ./src/index.js &
  
# Wait for any process to exit
wait -n
  
# Exit with status of process that exited first
exit $?